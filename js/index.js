window.onload = function() {
  let pixels = document.getElementsByClassName('js--pixel');
  const spheres = document.getElementsByClassName("js--sphere");
  const mix = document.getElementsByClassName("js--mix");
  const pop = document.getElementById("js--pop");
  const splash = document.getElementById("js--splash");
  const cursor = document.getElementById("js--cursor");
  const opslaanKnop = document.getElementById("js--opslaan");
  const backupKnop = document.getElementById("js--backup");
  const scene = document.getElementById("js--scene");
  let brush = "black";

  for (let i = 0; i < pixels.length; i++) {
    pixels[i].onmouseenter = (event) => {
      pixels[i].setAttribute('color', brush);
      splash.currentTime = 0.35;
      splash.play();
    };
  }

  opslaanKnop.onmouseenter = (event) => {
    opslaan();
  };

  backupKnop.onmouseenter = (event) => {
    backup();
  };

  function opslaan() {
    art = [];

    for (let i = 0; i < pixels.length; i++) {
      art.push(pixels[i].getAttribute("color"));
      pixels[i].setAttribute("color", "black");
    }
  }

  function backup() {
    for (let i = 0; i < pixels.length; i++) {
      pixels[i].setAttribute("color", art[i]);
    }
  }

  for (let i = 0; i < mix.length; i++) {
    if (mix[i].getAttribute('color') == "white") {
      mix[i].onmouseenter = (event) => {
        if (brush == "red") {
          mix[i].setAttribute('color', "#690618");
          console.log("hoi");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "green") {
          mix[i].setAttribute('color', "#085e0e");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "blue") {
          mix[i].setAttribute('color', "#08395e");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "yellow") {
          mix[i].setAttribute('color', "#727d0b");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "purple") {
          mix[i].setAttribute('color', "#570941");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "black") {
          mix[i].setAttribute('color', "#4a484a");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }
        pop.play();
      };
    }
    else if (mix[i].getAttribute('color') == "black") {
      mix[i].onmouseenter = (event) => {
        if (brush == "red") {
          mix[i].setAttribute('color', "#eb6779");
          console.log("hoi");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "green") {
          mix[i].setAttribute('color', "#7beb6a");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "blue") {
          mix[i].setAttribute('color', "#22e6e6");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "yellow") {
          mix[i].setAttribute('color', "#f8fcb8");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "purple") {
          mix[i].setAttribute('color', "#ec8ced");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "white") {
          mix[i].setAttribute('color', "#8c858a");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }
        pop.play();
      };
    }
    if (mix[i].getAttribute('color') == "blue") {
      mix[i].onmouseenter = (event) => {
        if (brush == "red") {
          mix[i].setAttribute('color', "#b634f7");
          console.log("hoi");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "green") {
          mix[i].setAttribute('color', "#468570");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "black") {
          mix[i].setAttribute('color', "#22e6e6");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "yellow") {
          mix[i].setAttribute('color', "#08395e");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "purple") {
          mix[i].setAttribute('color', "#773fd9");
          brush = spheres[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }else if (brush == "white") {
          mix[i].setAttribute('color', "#4a484a");
          brush = mix[i].getAttribute('color');
          cursor.setAttribute("color", brush);
        }
        pop.play();
      };
    }
  }

  for (let i = 0; i < spheres.length; i++) {
    spheres[i].onmouseenter = (event) => {
      brush = spheres[i].getAttribute('color');
      console.log(brush);
      cursor.setAttribute("color", brush);
      pop.play();
    };
  }
};
